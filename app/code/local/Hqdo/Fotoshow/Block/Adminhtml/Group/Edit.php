<?php
class Hqdo_Fotoshow_Block_Adminhtml_Group_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {
		$this->_objectId	=	'fotoshow_group_id';
		$this->_blockGroup	=	'hqdo_fotoshow';
		$this->_controller	=	'adminhtml_group';

		parent::__construct();

		$this->_updateButton('save', 'label', Mage::helper('hqdo_fotoshow')->__('Save slider group'));
		$this->_updateButton('delete', 'label', Mage::helper('hqdo_fotoshow')->__('Delete slider group'));
	}

	public function getHeaderText() {
		
		if (!Mage::registry('fotoshow_group')->getSliderGroupId()) {
			return Mage::helper('hqdo_fotoshow')->__('New block');
		}

		return Mage::helper('hqdo_fotoshow')->__("Edit fotoshow group '%s'", $this->escapeHtml(Mage::registry('fotoshow_group')->getTitle()));

	}
}