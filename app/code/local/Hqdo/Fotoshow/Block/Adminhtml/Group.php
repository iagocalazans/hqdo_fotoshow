<?php
class Hqdo_Fotoshow_Block_Adminhtml_Group extends Mage_Adminhtml_Block_Widget_Grid_Container {
	public function __construct() {
		$this->_blockGroup = 'hqdo_fotoshow';
		$this->_controller = 'adminhtml_group';
		$this->_headerText = Mage::helper('hqdo_fotoshow')->__('Manage groups');
		$this->_addButtonLabel = Mage::helper('hqdo_fotoshow')->__('Add new group');
		parent::__construct();
	}
}