<?php
class Hqdo_Fotoshow_Block_Adminhtml_Group_Grid extends Mage_Adminhtml_Block_Widget_Grid {
	public function __construct() {
		parent::__construct();
		$this->setId('fotoshowGroupGrid');
		$this->setDefaultSort('fotoshow_group_id');
		$this->setDefaultDir('ASC');
	}

	protected function _prepareCollection() {
		$collection = Mage::getModel('hqdo_fotoshow/group')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('fotoshow_group_id', array(
			'header'	=>	Mage::helper('hqdo_fotoshow')->__('ID'),
			'align'		=>	'left',
			'index'		=>	'fotoshow_group_id',
			'type'		=>	'number'
		));

		$this->addColumn('code', array(
			'header'	=>	Mage::helper('hqdo_fotoshow')->__('Code'),
			'align'		=>	'left',
			'index'		=>	'code'
		));

		$this->addColumn('title', array(
			'header'	=>	Mage::helper('hqdo_fotoshow')->__('Title'),
			'align'		=>	'left',
			'index'		=>	'title'
		));

		$this->addColumn('images', array(
			'header'	=>	Mage::helper('hqdo_fotoshow')->__('Images'),
			'align'		=>	'left',
			'index'		=>	'images',
			'renderer'	=>	'Hqdo_Fotoshow_Block_Adminhtml_Group_Grid_Renderer_Group_Image'
		));

		$this->addColumn('is_active', array(
			'header'	=>	Mage::helper('hqdo_fotoshow')->__('Status'),
			'index'		=>	'is_active',
			'type'		=>	'options',
			'options'	=>	array(
				Hqdo_Fotoshow_Model_Group::DISABLED
					=>	Mage::helper('hqdo_fotoshow')->__('Disabled'),
				Hqdo_Fotoshow_Model_Group::ENABLED
					=>	Mage::helper('hqdo_fotoshow')->__('Enabled')
			)
		));

		return parent::_prepareColumns() {
			return $this->getUrl('*/*/edit', array(
				'hqdo_fotoshow'	=>	$row->getId()
			));
		}
	}
}