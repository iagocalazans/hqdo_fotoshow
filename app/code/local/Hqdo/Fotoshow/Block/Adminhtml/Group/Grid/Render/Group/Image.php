<?php
class Hqdo_Fotoshow_Block_Adminhtml_Group_Grid_Render_Group_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Render_Abstract {

	public function render(Varien_Object $row) {
		return $this->_getValue($row);
	}
	
	protected function _getValue(Varien_Object $row) {
		$images = Mage::getModel('hqdo_fotoshow/images')->getImagesByGroup($row, false);

		if (!isset($images)) {
			return 'No images here';
		}

		$result = '<ul>';

		foreach ($images as $image) {
			$result .= '<li style="display: inline-block">';
			$block = Mage::app()->getLayout()->createBlock('hqdo_fotoshow/view');

			if (!getimagesize($block->getImageSrc($image))) {
				$result .= $image->getFilename();
				continue;
			}

			$result .= Mage::helper('hqdo_fotoshow')->getImage($image, 30);
			$result .= "<br /><label>{$row->getFilename()}</label";
			$result .= '</li>';
		}

		$result .= '</ul>';
		return $result;
	}
}